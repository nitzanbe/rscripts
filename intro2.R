x<-5
y<-4

x
y
x[1]<-6
x[2]<-4
#x[3]<-'car'

#expressions (VECROR OPEARATIONS)
y <-x*2
z <-x*2

#test data type
class(x)

#data type conversion
x<-as.character(x)
x<-as.integer(x)

#create vector with combine
x<-c(12,4,8)
x<-c(x,19,20)

#create vector with sequence
y<-1:6
z<-35:789
z[2]
z

#subseting vectors
v<-c(45,56,87,94,56,56,77)
w<-v[3:6]
z<-v[5]
z<-v[c(1,6,7)]
z<-v[v>60] #filter
t<-v>60

#naming vector items
salary<-c(8000,6000,4000,4000)
names(salary)<-c('jack','john','john','alice')
salary[1]
salary['jack']
salary['john']
salary[3]
names(salary)

#A general method create a sequence
s<-seq(4,9,length=7)

#random sampling
smp<-sample(v,10,replace = T)
smp1<-sample(v,5)
?sample

#counting mean
meanv<-mean(v)
v.with.na<-c(v,NA)
mean(v.with.na)#error
mean(v.with.na,na.rm = T)

#Handling.NAS
is.na(v.with.na)
filter.na<-is.na(v.with.na)
v.with.na[filter.na]
v.with.na[filter.na]

#functions

tofar<-function(cel=5){
  far<-cel*1.8+32
  return(far)
}


tofar(30)
tofar()


#sampling from distributions
x<-rnorm(10,11,2)
y<-runif(10,2,9)

#sapply
v<-c(1,11,13,4,5)

cut5<-function(x){
  if(x<5){
    return(x)
  }else{
    return(5)
  }
}

vmax5<-sapply(v, cut5)

#Monte carlo simulation

project.length<-function(x){
  a.time<-rnorm(1,10,2)
  b.time<-runif(1,5,13)
  c.time<-rnorm(1,16,3)
  if(a.time + b.time > c.time){
    project.time = a.time + b.time
  } else {
    project.time=c.time
  }
  return(project.time)
}

project.length(2)

samples<-1:10000
proc.time.vec<-sapply(samples,project.length)
hist(proc.time.vec,100)

#Matrices
v1<-c(1,2,3,4,5)
v2<-c(5,6,7,8,9)

mat1<-cbind(v1,v2)
mat2<-rbind(v1,v2)

class(mat1)
typeof(mat1)

t(mat1)

#applr function

mat<-matrix(rnorm(24,5,2),8,3)
colSums.means<-apply(mat,2,mean)
rows.means<-apply(mat,1,mean)

cut5.all<-apply(mat,c(1,2),cut5)





