
bikes.data.raw <- read.csv("bike sharing.csv")
library(ggplot2)
bikes.data.prepared <- bikes.data.raw
bikes.data.prepared$datetime <- as.character(bikes.data.prepared$datetime)
#days
Sys.setlocale("LC_TIME", "English")
date <- substr(bikes.data.prepared$datetime,1,10)
days <- weekdays(as.Date(date))
bikes.data.prepared$weekday <- as.factor(days)
#month
month <- as.integer(substr(date,6,7))
bikes.data.prepared$month <- month
#year
year <- as.integer(substr(date,1,4))
bikes.data.prepared$year <- year
#hour
hour <- as.integer(substr(bikes.data.prepared$datetime,12,13))
bikes.data.prepared$hour <- hour
#season
table(bikes.data.prepared$season,bikes.data.prepared$month)
bikes.data.prepared$season <- factor(bikes.data.prepared$season, levels = 1:4, labels = c('winter','spring','summer','autumn'))
#weather
bikes.data.prepared$weather <- factor(bikes.data.prepared$weather, levels = 1:4, labels = c('clear','mist','light snow','heavy rain'))

#BoxplotSeason
ggplot(bikes.data.prepared, aes(x=season, y=count)) + 
      labs(title="Impact of seasons on bicycle rental data",x="season", y = "count")+
      geom_boxplot()

#BoxplotWeather
ggplot(bikes.data.prepared, aes(x=weather, y=count)) + 
      labs(title="Impact of weather on bicycle rental data",x="weather", y = "count")+
      geom_boxplot()
