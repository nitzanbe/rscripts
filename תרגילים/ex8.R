loan.raw <- read.csv("loans_data.csv")
library(ggplot2)
library(caTools)

loan.preapared <- loan.raw
loan.preapared$Loan_ID <- NULL
loan.preapared$Gender <- as.character(loan.preapared$Gender)

make_male <- function(x){
  if (x=="") return('Male')
  return(x)
}

loan.preapared$Gender <- sapply(loan.preapared$Gender,make_male)
loan.preapared$Gender <- as.factor(loan.preapared$Gender)
loan.preapared$Married <- as.character(loan.preapared$Married)

make_yes <- function(x){
  if (x=="") return('Yes')
  return(x)
}

loan.preapared$Married <- sapply(loan.preapared$Married,make_yes)
loan.preapared$Married <- as.factor(loan.preapared$Married)
loan.preapared$Dependents <- as.character(loan.preapared$Dependents)

make_numbers <- function(x){
  if (x=="3+") return(3)
  if (x=="") return(0)
  return(x)
}

loan.preapared$Dependents <- sapply(loan.preapared$Dependents, make_numbers)
loan.preapared$Dependents <- as.integer(loan.preapared$Dependents)
loan.preapared$Self_Employed <- as.character(loan.preapared$Self_Employed)

make_unknown <- function(x){
  if (x=="") return('Unknown')
  return(x)
}

loan.preapared$Self_Employed <- sapply(loan.preapared$Self_Employed,make_unknown)
loan.preapared$Self_Employed <- as.factor(loan.preapared$Self_Employed)

make_average <- function(x,vec){
  if (is.na(x)){
    return(mean(vec,na.rm = T))
  }
  return(x)
}

loan.preapared$LoanAmount <- sapply(loan.preapared$LoanAmount, make_average, vec = loan.preapared$LoanAmount)
loan.preapared$Loan_Amount_Term <- sapply(loan.preapared$Loan_Amount_Term, make_average, vec = loan.preapared$Loan_Amount_Term)

make_1 <- function(x){
  if (is.na(x)){
    return(1)
  }
  return(x)
}

loan.preapared$Credit_History <- sapply(loan.preapared$Credit_History, make_1)
filter <- sample.split(loan.preapared$Loan_Status, SplitRatio = 0.7)
loan.train <- subset(loan.preapared, filter == TRUE)
loan.test <- subset(loan.preapared, filter == FALSE)
loan.model <- glm(Loan_Status ~ ., family = binomial(link = "logit"), data = loan.train)
predicted.loan.test <- predict(loan.model, newdata = loan.test, type = 'response')

make_min <- function(price_NT,price_YF){
  confusion_matrix <- table(loan.test$Loan_Status, predicted.loan.test > 0)
  if(ncol(confusion_matrix)==1){
    totalmin<-price_NT*confusion_matrix[1,1]
  }else{
    totalmin<-price_NT*confusion_matrix[1,2]+price_YF*confusion_matrix[2,1]
  }
  min<-0
  i<-0.01
  while (i<=1){
    confusion_matrix1 <- table(loan.test$Loan_Status, predicted.loan.test > i)
    if(ncol(confusion_matrix1)==1){
      total<-price_NT*confusion_matrix1[1,1]
    }else{
      total<-price_NT*confusion_matrix1[1,2]+price_YF*confusion_matrix1[2,1]
    }
    if(total<totalmin){
      totalmin<-total
      confusion_matrix<-confusion_matrix1
      min<-i
    }
    i<-i+0.01
  }
  return(min)
}

make_min(70,30)